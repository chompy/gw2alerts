"""
GW2 Alerts -- A Guild Wars2 Event Notifier
By Nathan Ogden (Chompy)
-----------------------------------------------------
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------
Please see gw2Alerts.py for more information

This file contains code taken from Stack Overflow user 'FogleBird' @
http://stackoverflow.com/questions/6389580/quick-and-easy-trayicon-with-python

"""
# import modules
import wx, sys, base64

# import gw2Alerts modules
import gw2EmbeddedAssets

def create_menu_item(menu, label, func):
  item = wx.MenuItem(menu, -1, label)
  menu.Bind(wx.EVT_MENU, func, id=item.GetId())
  menu.AppendItem(item)
  return item        
        
class TaskBarIcon(wx.TaskBarIcon):
  def __init__(self):
    super(TaskBarIcon, self).__init__()

    icon = wx.ImageFromStream(gw2EmbeddedAssets.ICON)
    icon = wx.IconFromBitmap(wx.BitmapFromImage(icon))
    self.SetIcon(icon, "GW2 Event Alerter v%s -- By Nathan 'Chompy' Ogden" % (str(gw2EmbeddedAssets.VERSION)))    
    
    self.Bind(wx.EVT_TASKBAR_LEFT_DOWN, self.on_left_down)

  def CreatePopupMenu(self):
    menu = wx.Menu()
    #create_menu_item(menu, 'Check for Updates', self.on_exit)
    #menu.AppendSeparator()
    create_menu_item(menu, 'Exit', self.on_exit)
    return menu

  def on_left_down(self, event):
    return

  def on_about(self, event):
    return
    
  def on_exit(self, event):
    wx.CallAfter(self.Destroy)
    sys.exit()