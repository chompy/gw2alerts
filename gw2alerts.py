"""
GW2 Alerts -- A Guild Wars2 Event Notifier
By Nathan Ogden (Chompy)
-----------------------------------------------------
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------
This source uses ToasterBox by Andrea Gavana <andrea.gavana@gmail.com>
ToasterBox is distributed under the wxPython license.

This software requires the following Python modules...

ToasterBox (included).
PyYAML - http://pyyaml.org/wiki/PyYAML
wX (Usually included with Python)

Optional...

pyttsx - https://pypi.python.org/pypi/pyttsx/1.0

"""

# import gw2Alerts modules
import gw2TaskIcon, gw2EmbeddedAssets

# import toasterbox and wx
import wx
import ToasterBox as TB

# urllib
import urllib2
opener = urllib2.build_opener()

# json
import json

# core modules
import sys, time, os

# log method, prints info to console
def log(msg, type = "INFO"):
  print "[%s] [%s] %s" % (type, time.strftime("%m/%d/%Y %H:%M:%S"), msg)
  return

# init log to console
log("Init GW2 event notifier.")

# pyttsx
try:
  TXT2SPX = True
  import pyttsx
  log("Pyttsx loaded.")
except:
  TXT2SPX = False
  log("Pyttsx module not found.", "WARN")

# yaml
import yaml
CONF = {}
if os.path.exists("gw2alerts.yaml"):
  f = open("gw2alerts.yaml", "r")
  CONF = yaml.load(f.read())
  f.close()
  log("Config file read.")

class GW2Alerts(wx.Frame):

    def __init__(self, worldId, id=wx.ID_ANY, title="GW2 Alerts",
                 pos = wx.DefaultPosition, size=(400,550)):
      wx.Frame.__init__(self, None, id, title, pos, size)
      self.events = []
      
      # start taskbar icon
      self.taskbar = gw2TaskIcon.TaskBarIcon()
      
      # set world id
      self.worldId = worldId
      log("World ID is %s." % (str(worldId)))
      
      # get list of event names
      get = opener.open("https://api.guildwars2.com/v1/event_names.json")
      if not get: sys.exit()
      
      # get list of event names
      self.eventNames = json.loads(get.read())      
      log("Retrived list of event names from GW2 API.")
      
      # load alert sound
      self.alertSfx = None
      if 'alert_sound' in CONF and CONF['alert_sound'] and os.path.exists(CONF['alert_sound']):
        self.alertSfx = wx.Sound("alert.wav" if not 'alert_sound' in CONF else CONF['alert_sound'])
        log("Notification sound loaded.")
      else:
        self.alertSfx = wx.Sound()
        self.alertSfx.CreateFromData(gw2EmbeddedAssets.ALERT_SFX)   
        log("Notification sound loaded.")
                    
      # text to speech engine
      self.txt2spx = None
      if TXT2SPX and 'use_text_to_speech' in CONF and CONF['use_text_to_speech']:
        try:
          self.txt2spx = pyttsx.init()
          log("Pyttsx (text-to-speech) module has been initalized.")
        except:
          log("Failed to initialize pyttsx (text-to-speech) module.", "WARN")
        
            
      # add events
      if 'events' in CONF:
        for alerts in CONF['events']:
          self.addEvent(alerts, CONF['events'][alerts])            
            
      # api check timer
      self.timer = wx.Timer(self)
      self.Bind(wx.EVT_TIMER, self.apiCheck, self.timer)
      self.timer.Start(10000)
      log("API check timer started.")
      return

    def popup(self, msg = "No messages here."):

        tb = TB.ToasterBox(self, 1, 33587202, 1)

        sizex = 210
        sizey = 130
        tb.SetPopupSize((sizex, sizey))

        winSize = wx.DisplaySize()
        
        posx = winSize[0] - 210 - 25
        posy = winSize[1] - 130
        tb.SetPopupPosition((posx, posy))
        
        tb.SetPopupPauseTime(8000)
        tb.SetPopupScrollSpeed(4)

        tb.SetPopupBackgroundColor((255,255,255))
        tb.SetPopupTextColor((0,0,0))
        
        bg = None if not 'alert_bg' in CONF else CONF['alert_bg']
        
        if bg:
          dummybmp = wx.Bitmap(bg, wx.BITMAP_TYPE_BMP)

          if dummybmp.Ok():
              tb.SetPopupBitmap(bg)
          else:
              tb.SetPopupBitmap()
             
        tb.SetPopupText(msg)
        tb.Play()       

    def apiCheck(self, event):
      """ GW2 API event check loop. """
      log("TICK")
      
      # make sure we have events to check
      if len(self.events) >= 0:
        # attempt to open api connection
        get = opener.open("https://api.guildwars2.com/v1/events.json?world_id=" + str(self.worldId))
        # failure
        if not get:
          self.popup("Failed to open API connection.")       
          return False
      
        # success
        events = json.loads(get.read())
        
        # find events
        for event in events['events']:
          for i in range(len(self.events)):
            # found event
            if self.events[i]['id'] == event['event_id']:
              if event['state'] == self.events[i]['alertState'] and time.time() - self.events[i]['repeat'] > self.events[i]['lastSpawn']:
                # log
                log("Event '" + self.events[i]['name'] + "' is " + event['state'].lower() + ".")
                # alert popup
                self.popup("Event '" + self.events[i]['name'] + "' is " + event['state'].lower() + ".")
                # alert sfx
                if self.alertSfx:
                  self.alertSfx.Play(wx.SOUND_ASYNC)                
                # text to speech
                if self.txt2spx:
                  self.txt2spx.say("Event '" + self.events[i]['name'] + "' is " + event['state'].lower())
                  self.txt2spx.runAndWait()                
                # timestamp this event so it user doesn't get notified again
                self.events[i]['lastSpawn'] = time.time()
      
      log("TOCK")
      return True
      
    def addEvent(self, id, alertState = "Active"):
      """ Add new event to event watcher. """

      # check event name list for this event
      for event in self.eventNames:
        if event['id'] == id:
          self.events.append({'id' : id, 'name' : event['name'], 'repeat': 3600, 'lastSpawn' : 0, 'alertState' : alertState})
          self.popup("Added event " + id + " (" + event['name'] + ") to trigger when state is " + alertState.lower() + ".")
          log("Added event " + id + " (" + event['name'] + ") to trigger when state is " + alertState.lower() + ".")
          return True
      return False
        

        
def main():

    app = wx.PySimpleApp()
    
    # start gw2alerts module with world 1003 (yak's bend)
    gw2 = GW2Alerts(CONF['world_id'] if 'world_id' in CONF else 1003) # 1003 = Yak's bend
        
    # main loop
    app.MainLoop()   
    
if __name__ == "__main__":
    main()
      