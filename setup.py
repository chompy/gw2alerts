from distutils.core import setup
import py2exe, sys, os
sys.argv.append('py2exe')

setup(
    options = {'py2exe': {'bundle_files': 1, 'compressed': True}},
    includes = "pyttsx.drivers.sapi5",
    windows = [{'script': "gw2alerts.py", "icon_resources": [(1, "icon.ico")]}],
    zipfile = None,
)